@tool
class_name BufferedHBox
extends HBoxContainer


@export var left_ratio = 1.0
@export var center_ratio = 1.0
@export var right_ratio = 1.0

var leftBuffer:Control
var rightBuffer:Control
var centerBuffer:Control


func _ready():
	connect("child_entered_tree",setDirty)
	connect("child_exiting_tree",setDirty)
	setDirty()
#		(i as Node).owner = owner

var dirty = false
func setDirty(flag=null):
	if flag and (flag == leftBuffer or flag == rightBuffer):
		pass
	dirty = true;
	
func _process(_delta):
	if dirty: rebuild()
	
	if is_instance_valid(leftBuffer) and leftBuffer.size_flags_stretch_ratio != left_ratio:
		leftBuffer.size_flags_stretch_ratio = left_ratio

	if is_instance_valid(rightBuffer) and rightBuffer.size_flags_stretch_ratio != right_ratio:
		rightBuffer.size_flags_stretch_ratio = right_ratio
		
	if is_instance_valid(centerBuffer) and centerBuffer.size_flags_stretch_ratio != center_ratio:
		centerBuffer.size_flags_stretch_ratio = center_ratio

func clean():
	if is_instance_valid(leftBuffer):
		leftBuffer.free()
		leftBuffer = null
	if is_instance_valid(rightBuffer):
		rightBuffer.free()
		rightBuffer = null



func rebuild():
	clean()
	add_theme_constant_override("separation",0)
	
	if get_child_count() != 1 or (get_child(0) is Control == false):
		error = "Expects exactly 1 Control Node child"
		update_configuration_warnings()
		return
	else:
		error = ""
		update_configuration_warnings()
	centerBuffer = get_child(0)



	leftBuffer = Control.new()
	leftBuffer.name = "left"
	leftBuffer.add_to_group("buffer")

	rightBuffer = Control.new()
	rightBuffer.name = "right"
	rightBuffer.add_to_group("buffer")

	add_child(leftBuffer)
	add_child(rightBuffer)

	move_child(leftBuffer,0)
	move_child(centerBuffer,1)
	move_child(rightBuffer,2)

	for i in get_children():
		print("i = ", i.name)
		i.size_flags_horizontal = Control.SIZE_EXPAND_FILL
		i.size_flags_vertical = Control.SIZE_EXPAND_FILL

 #		i.owner = owner
	dirty = false

var error = ""
func _get_configuration_warning():
	return PackedStringArray([error])
