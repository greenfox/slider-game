# Slider Game

Okay, so this game is about Taxes. Exciting, I know.

But it goes quick. Basically, you're only given one slider and an "okay" button.

The slider is how you invest your previous year's profits.

You could could it and invest in stocks.
Or you could invest it in your business.

If you invest in stocks, you have to eat the corporate taxes.
If you invest in your business, your business is taxed less.

Every year that you invest in your business, it grows.
Every year you invest in stocks, they grow.

The slider is a gimmick. The optimum strategy is always to move the slider all the way to one side or the other. When taxes are high, the optimum strategy is always to invest in your business. When taxes are low, the optimum strategy is always to invest in stocks.

The game doesn't tell you this until the end.




# Intro

This is your business. You make $1,000,000 in revenue every year. About 90% of that goes to making products and paying employees. This includes your paycheck. You have decided to